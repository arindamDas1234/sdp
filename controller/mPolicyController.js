const fs = require("fs");

const db = require("../models");

// ...rest of the initial code omitted for simplicity.
const { body, validationResult,  } = require('express-validator');

exports.createPolicy = (req,res) =>{
req.body.createdDate = new Date(); 
  const errors = validationResult(req);
 console.log(errors);
	if(!errors.isEmpty()){
		res.status(422).json({
			errors:errors.array()
		});
	}else{
		db.mPolicyes.create({
			policyName:req.body.policyName,
			createdBy:req.body.createdBy,
			createdDate:req.body.createdDate
		}).then(result =>{
			res.status(200).json({
				message:"Policy created successfully",
				status:true
			});
		}).catch(err =>{
			console.log(err);

			res.status(422).json({
				message:"Policy  not created successfully ",
				status:false
			});
		});

		
	}
}

exports.editPolicy = (req,res) =>{
	let id = req.params.id;

	db.mPolicyes.findOne({where:{id:id}})

	.then(response =>{
		if(response){
			res.status(200).json({
				policyes:response,
				status:true
			});
		}
	}).catch(err =>{
		console.log(err);

		res.status(422).json({
			message:"Policyes not found",
			status:false
		});
	});
};

exports.deltePolicyes = (req,res) =>{
	db.mPolicyes.destroy({where:{id:req.params.id}})

	.then(response =>{
		res.status(200).json({
			message:"Poliyes  deleted successfully",
			status:true
		});
	}).catch(err =>{
		console.log(err);

		res.status(422).json({
			message:"Policyes not deleted fail"
		});
	});
};

exports.updatePolicyes = (req,res) =>{
	
	let id = req.params.id;
	req.body.updateDate = new Date();
	db.mPolicyes.update({
		policyName:req.body.policyName,
		updateBy:req.body.updatedBy,
		updateDate:req.body.updateDate
	},{where:{id:id}})
	.then(result =>{
		if(result){
			res.status(200).json({
				message:"Poliyes updated successfully",
				status:true
			});
		}else{
			res.status(422).json({
				message:"Plocyes updated not updated fail",
				status:false
			});
		}

	}).catch(error =>{
		console.log(error);

		res.status(422).json({
			message:"Polyes not udated successfully",
			status:false
		});
	});
}