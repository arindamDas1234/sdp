module.exports = (sequelize, DataTypes) =>{
    const mPolicyesRule = sequelize.define("mPolicyesRules",{
        id:{
            type:DataTypes.INTEGER,
            primaryKey:true,
            autoIncrement:true
        },
        rulesName:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:false
            }
        },
        rule:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:false
            }
        },
        createdBy:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:false
            }
        },
        updateBy:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:false
            }
        },
        created_at:{
            type:DataTypes.DATE,
            default:sequelize.NOW

        },
        updated_at:{
            type:DataTypes.DATE,
            allowNull:false,
            validate:{
                notEmpty:false
            }
        },
    });

    return mPolicyesRule;
}