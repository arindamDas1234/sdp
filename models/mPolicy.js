module.exports = (sequelize, DataTypes) =>{
    const mPolicy = sequelize.define('mPolicyes',{
        id:{
            type:DataTypes.INTEGER,
            autoIncrement:true,
            primaryKey:true
        },
        policyName:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:true
            }
        },
        createdBy:{
            type:DataTypes.STRING,
            allowNull:false,
            validate:{
                notEmpty:true
            }
        },
        updateBy:{
            type:DataTypes.STRING,
            allowNull:true,
         
        },
        createdDate:{
            type:DataTypes.DATE,
            default:sequelize.NOW
        },
        updateDate:{
            type:DataTypes.DATE,
            allowNull:true
        }

    });

    return mPolicy;
}