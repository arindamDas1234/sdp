const express = require("express");

const router = express.Router();
const controller = require("../controller/mPolicyController.js");

// ...rest of the initial code omitted for simplicity.
const { body, validationResult, check } = require('express-validator');

router.get('/api/creat_new_policy', function(req,res,next){
    console.log(req.url);
});
// create new Policyes
router.post("/api/create_new_policy",[check("policyName").notEmpty().withMessage("Policy name is required"), check("createdBy").notEmpty().withMessage("created by is requried"),
], controller.createPolicy);
// edit new Policyes
router.get("/api/editPolicy/:id", controller.editPolicy );
// delete new Polyes
router.delete("/api/delete_newPolicyes/:id", controller.deltePolicyes);
// update policyname



module.exports = router;