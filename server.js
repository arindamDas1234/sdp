const express = require("express");
const cors = require("cors");

const cluster = require("cluster");
const os = require("os");
const numberCpu = os.cpus();
const bunyan = require("bunyan");
const http =require("http");

require("dotenv").config();

const logger = bunyan.createLogger({name:"main file"});
const router = require("./router/router.js");
const bodyParser = require("body-parser");
const db = require("./models/index.js");

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended:false
}));
app.use(cors());

app.set("views", __dirname+"/views");
app.set("view engine", 'ejs');

app.use("/", router);

if(cluster.isMaster){
	for(let i=0; i < numberCpu.length; i++){
		cluster.fork();
	}
	// get more number of workers
	let workerThred = [];
	for(const id in cluster.workers){
		workerThred.push(id);
	}
	// send message to workers 
	workerThred.forEach(async pid =>{
 
	 	cluster.workers[pid].send({
			from:"isMaster",
			type:'SIGKILL',
			message:"Hellow this is isMaster"
		});
	});

	// check worker status 
	if(process.env.NODE_ENV == "production"){
		 cluster.on("online", function(worker){
		 	logger.info(`${worker.process.pid} is now online`);
		 });

		 cluster.on("exit", function(worker,signal, code){
		 	if(worker.isDead()){
		 		logger.info(`${worker.process.pid} is now dead Please clean up `);
		 	}
		 	cluster.fork();
		 });

		 cluster.on("message", function(worker, {from, message}){

		 	logger.info(`The message is form ${worker.process.pid} and message is ${message}`);
		 });
	}
}else{


		console.log("yes iam worker");
		process.on("message", function(msg){
			if(msg.type === "SIGKILL" && msg.from ==="isMaster"){
				process.send({
					from:"isWorker",
					message:`yes master ${msg.from}`
				});
			}
		});

		process.on("exit", function(){
			process.exit(process.exitCode);
		});

		let PORT = process.env.PORT;

		const server = http.createServer(app);
		db.sequelize.sync().then(req =>{
			server.listen(PORT, function(){
				console.log("server run on port "+PORT);
				logger.info("server running on port "+PORT);
	
			});
		})

}